package com.mycompany.labox_02;

import java.util.Scanner;

public class LabOX_02 {

    private static char[][] board;
    private static char currentPlayer = 'X';

    public static void main(String[] args) {
        initializeBoard();
        printBoard();
        playGame();
    }

    private static void initializeBoard() {
        board = new char[3][3];
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = (char) ((row * 3) + col + 1 + '0');
            }
        }
    }

    private static void printBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print("| " + board[row][col] + " ");
            }
            System.out.println("|");
            System.out.println("-------------");
        }
    }

    private static void playGame() {
        Scanner kb = new Scanner(System.in);
        int position;
        boolean gameOver = false;

        while (!gameOver) {
            System.out.println("Player " + currentPlayer + ", enter position (1-9): ");
            position = kb.nextInt();

            if (isValidMove(position)) {
                makeMove(position);
                printBoard();

                if (checkWin()) {
                    System.out.println("Player " + currentPlayer + " wins!");
                    gameOver = true;
                } else if (isBoardFull()) {
                    System.out.println("It's a tie!");
                    gameOver = true;
                } else {
                    switchPlayer();
                }
            } else {
                System.out.println("Invalid move. The cell is already taken or the input is not in the range 1-9.");
            }
        }

        kb.close();
    }

    private static boolean isValidMove(int position) {
        return position >= 1 && position <= 9 && board[(position - 1) / 3][(position - 1) % 3] == (char)(position + '0');
    }

    private static void makeMove(int position) {
        int row = (position - 1) / 3;
        int col = (position - 1) % 3;
        board[row][col] = currentPlayer;
    }

    private static boolean checkWin() {
        return checkRows() || checkColumns() || checkCross();
    }

    private static boolean checkRows() {
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == currentPlayer && board[row][0] == board[row][1] && board[row][0] == board[row][2]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkColumns() {
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == currentPlayer && board[0][col] == board[1][col] && board[0][col] == board[2][col]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCross() {
        if (board[0][0] == currentPlayer && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            return true;
        }
        return board[0][2] == currentPlayer && board[0][2] == board[1][1] && board[0][2] == board[2][0];
    }

    private static boolean isBoardFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] != 'X' && board[row][col] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void switchPlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }
}